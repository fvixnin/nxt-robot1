#include "opencv2/opencv.hpp"
#include "iostream"

using namespace cv;
using namespace std;

void YellPlane(Mat img1, Mat &imgYellow)
{
    Scalar yell1 = Scalar(15, 10, 10);
    Scalar yell2 = Scalar(35, 255, 255);

    Mat imgHSV;
    cvtColor(img1, imgHSV, COLOR_BGR2HSV);
    inRange(imgHSV, yell1, yell2, imgYellow);
}

void BluePlane(Mat img1, Mat &imgYellow)
{
    Scalar blue1 = Scalar(80, 10, 10);
    Scalar blue2 = Scalar(100, 255, 255);

    Mat imgHSV;
    cvtColor(img1, imgHSV, COLOR_BGR2HSV);
    inRange(imgHSV, blue1, blue2, imgYellow);
}

void RedPlane(Mat img1, Mat &imgYellow)
{
    Scalar red1 = Scalar(155, 10, 10);
    Scalar red2 = Scalar(180, 255, 255);

    Mat imgHSV;
    cvtColor(img1, imgHSV, COLOR_BGR2HSV);
    inRange(imgHSV, red1, red2, imgYellow);
}
Point2f point_search(Mat imgHSV)
{

    Moments oMoments = moments(imgHSV);

    //for cleaner acess
    double dM01 = oMoments.m01;
    double dM10 = oMoments.m10;
    double dArea = oMoments.m00;

    //if there are a lot of red pixels the object is in the image

    //calculate coordintes of the object
    int posX = dM10 / dArea;
    int posY = dM01 / dArea;
    return Point2f(posX, posY);
}

double sc_multiplication(Vec2f vec1, Vec2f vec2)
{
    if ((vec1[0] + vec1[1] != 0) && (vec2[0] + vec2[1] != 0))
    {
        return (vec1[0] * vec2[1] + vec1[1] * vec2[0]) / (sqrt(vec1[0] * vec1[0] + vec1[1] * vec1[1]) * sqrt(vec2[0] * vec2[0] + vec2[1] * vec2[1]));
    }
    else
        return CV_PI;
}
int main()
{
    VideoCapture cap(0);
    Mat img;
    Mat imgYellow;
    Mat imgBlue;
    Vec2f Vertical = (0, 1);
    Vec2f Horizontal = (1, 0);

    double angle = CV_PI, angle0 = 0;
    bool whyrurunin = false;
    bool shouldrotate = true;
    bool shouldgo = false;
    Point2f Goal, Position;
    cin >> Goal.x >> Goal.y;
    char data[10];
    ofstream f;
    f.open("");
    while (1)
    {
        /* buttom = cvWaitKey(1);
        if ( buttom == 'w' ) UB+=5;
        else if ( buttom == 's' ) UB-=5;
        else if ( buttom == 'd' ) DB+=5;
        else if ( buttom == 'a' ) DB-=5;   Полезная вещь */
        /*
        xddd lets try*/
        cap >> img;
        imgYellow = img;
        imgBlue = img;
        YellPlane(img, imgYellow);
        BluePlane(img, imgBlue);
        /*imgYellow.at<Vec3b>(point_search(imgYellow).y, point_search(imgYellow).x)[0]= 0;
        imgYellow.at<Vec3b>(point_search(imgYellow).y, point_search(imgYellow).x)[1]= 255;
        imgYellow.at<Vec3b>(point_search(imgYellow).y, point_search(imgYellow).x)[2]= 255;
        imgBlue.at<Vec3b>(point_search(imgBlue).y, point_search(imgBlue).x)[0]= 0;
        imgBlue.at<Vec3b>(point_search(imgBlue).y, point_search(imgBlue).x)[1]= 255;
        imgBlue.at<Vec3b>(point_search(imgBlue).y, point_search(imgBlue).x)[2]= 255;
        */
        circle(img, point_search(imgYellow), 4, Scalar(0, 255, 255), 1, 15);
        circle(img, point_search(imgBlue), 4, Scalar(255, 0, 0), 1, 15);

        imshow("VIEW", img);
        angle0 = angle;
        Position = (point_search(imgYellow) + point_search(imgBlue)) / 2;
        Vec2f Direction = point_search(imgYellow) - point_search(imgBlue);
        float s = Direction[0];
        Direction[0] = -Direction[1];
        Direction[1] = s;
        Vec2f Way = Goal - Position;

        if (sc_multiplication(Direction, Way) != CV_PI)
        {
            angle = acos(sc_multiplication(Direction, Way));
        }
        else
        {
            cout << "Error of angle" << endl;
            whyrurunin = true;
        }
        if (angle == 0)
        {
            !shouldrotate;
            data[0] = 10;
            data[1] = 90;
        }
        else
        {
            float k = (float)(angle * 10);
            data[2] = 2;
            data[0] = k;
        }
        int a = 100;
        if (Position == Goal)
            break;
        else
        {

        }

        if (!whyrurunin)
            waitKey(1);
        else
            break;

        /* if (waitKey(1) > 0)
            return 0; */
    }
    f.close();
}